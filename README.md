# telemetryoverlay

A simple HUD overlay filter for frei0r.
Add this directory under frei0r/src/filter, then add the directory to the cairo test in frei0r/src/filter/CMakeLists.txt.
Once it's built copy the frei0r_telemetryoverly.xml file to /usr/share/kdenlive/effects so the UI works.
Use the keyframeable parameter "Telemetry Time Offset" to align the telemetry to the video.
The rest is pretty self-explanatory.

Use this code however you wish.