
#include "telemetry.h"
#include "track.h"
#include "racedashlog.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <vector>
#include <algorithm>

//---------------------------------------------------------
Telemetry::Telemetry(void)
{
    Unload();
}

//---------------------------------------------------------
Telemetry::~Telemetry()
{

}

//---------------------------------------------------------
void Telemetry::Unload(void)
{
    memset(mLoadedFile, 0, PATH_MAX);
    mLaps.clear();
    mSamples.clear();
    mUpdatedSectors = false;
    mSectorsGood = false;
}

//---------------------------------------------------------
bool Telemetry::Load(const char *fname)
{
    if (fname == NULL) return false;
    if (strlen(fname) < 5) return false;

    // If we've already got this file loaded we're done.
    if (0 == strcmp(fname, mLoadedFile)) return mSamples.size() > 0;

    printf("Set fname to %s\n", fname);
    unsigned len = strlen(fname) - 4;
    printf("Set fname to %s, end is %s\n", fname, fname+len);
    //if (NULL != strstr(fname, ".rdl"))
    //{
    //   return LoadRacedash(fname);
    //}

    //return LoadSoloExport(fname);
    return LoadRacedash(fname);
}

//---------------------------------------------------------
bool Telemetry::LoadSoloExport(const char *fname)
{
    Unload();

    char line[1024];

    strcpy(mLoadedFile, fname);

    FILE *inf = fopen(fname, "rt");
    if (inf == NULL)
    {
        printf("Cannot open %s for reading.\n", fname);
        return false;
    }

    const char *strings[32];

    // Load and parse the header
    char *ret = fgets(line, 1023, inf);
    if (ret == NULL) return false;

    unsigned numStrings = ExtractStrings(strings, 32, line);

    unsigned lapIndex     = 0;  // No header, always first!
    unsigned lapDurIndex  = FindStringIndex(strings, numStrings, "Time");

    unsigned timeIndex  = FindStringIndex(strings, numStrings, "Time (s)");
    unsigned distIndex  = FindStringIndex(strings, numStrings, "Distance [feet]");
    unsigned speedIndex = FindStringIndex(strings, numStrings, "GPS_Speed [mph]");
    unsigned accXIndex  = FindStringIndex(strings, numStrings, "GPS_LatAcc [g]");
    unsigned accYIndex  = FindStringIndex(strings, numStrings, "GPS_LonAcc [g]");

    unsigned latIndex   = FindStringIndex(strings, numStrings, "GPS_Latitude [�]");
    unsigned lonIndex   = FindStringIndex(strings, numStrings, "GPS_Longitude [�]");
    unsigned eleIndex   = FindStringIndex(strings, numStrings, "GPS_Elevation [cm]");
    unsigned headIndex  = FindStringIndex(strings, numStrings, "GPS_Heading [deg]");

    if (lapDurIndex > numStrings) { fclose(inf); return false; }
    if (timeIndex > numStrings)   { fclose(inf); return false; }
    if (distIndex > numStrings)   { fclose(inf); return false; }
    if (speedIndex > numStrings)  { fclose(inf); return false; }
    if (accXIndex > numStrings)   { fclose(inf); return false; }
    if (accYIndex > numStrings)   { fclose(inf); return false; }
    if (latIndex > numStrings)    { fclose(inf); return false; }
    if (lonIndex > numStrings)    { fclose(inf); return false; }
    if (eleIndex > numStrings)    { fclose(inf); return false; }
    if (headIndex > numStrings)   { fclose(inf); return false; }


    TelemetryLap currentLap;
    TelemetrySample currentSample;

    memset(&currentLap, 0, sizeof(currentLap));

    unsigned currentLine = 1;
    double lapCumulativeTime = 0.0;

    while (!feof(inf))
    {
        ++currentLine;
        char *ret = fgets(line, 1023, inf);
        if (ret == NULL) continue;
        numStrings = ExtractStrings(strings, 32, line);

        if (numStrings == 0) break;

        // Do we have a new lap?
        if (strlen(strings[lapIndex]) > 0)
        {
            if (currentLap.mLap != 0)
            {
                mLaps.push_back(currentLap);
            }

            unsigned lap = atoi(&strings[lapIndex][5]); // skip the 'Lap: ' part of the string
            if (lap == 0)
            {
                printf("Error extracting lap on line %d  '%s'\n", currentLine, strings[lapIndex]);
                fclose(inf);
                return false;
            }

            currentLap.mLap         = lap;
            currentLap.mDuration    = GetSeconds(strings[lapDurIndex]);
            currentLap.mFirstSample = mSamples.size();
            currentLap.mNumSamples  = 0;
            currentLap.mStartTime   = lapCumulativeTime;
            lapCumulativeTime += currentLap.mDuration;
        }

        currentSample.mLap            = currentLap.mLap;
        currentSample.mLapTime        = atof(strings[timeIndex]);
        currentSample.mCumulativeTime = currentLap.mStartTime + currentSample.mLapTime;
        currentSample.mLatitude       = atof(strings[latIndex]);
        currentSample.mLongitude      = atof(strings[lonIndex]);
        currentSample.mElevation      = atof(strings[eleIndex]);
        currentSample.mSpeed          = atof(strings[speedIndex]);
        currentSample.mHeading        = atof(strings[headIndex]);
        currentSample.mAccelX         = atof(strings[accXIndex]);
        currentSample.mAccelY         = atof(strings[accYIndex]);

        ++currentLap.mNumSamples;
        mSamples.push_back(currentSample);
    }

    if (currentLap.mLap != mLaps.back().mLap)
    {
        mLaps.push_back(currentLap);
    }

    fclose(inf);

    printf("Loaded %d laps, %d samples\n", (int)mLaps.size(), (int)mSamples.size());

    CleanupHeading(7.0);

    return true;
}

//---------------------------------------------------------
bool Telemetry::LoadRacedash(const char *fname)
{
    Unload();

    strcpy(mLoadedFile, fname);

    RacedashLog rdl(fname);
    if (!rdl.IsValid()) return false;

    // Go through the loaded log and turn it into TelemetrySamples and TelemetryLaps

    // First, create a sorted array of all time indices
    std::vector<double> times;
    times.reserve(rdl.TotalTimes());
    unsigned item, a;
    for (item = 0; item < rdl.NumItems(); item++)
    {
        for (a = 0; a < rdl.ItemSamples(item); a++)
        {
            times.push_back(rdl.ItemSample(item, a).time);
        }
    }
    std::sort(times.begin(), times.end());

    // Now make them unique (down to a millisecond)
    unsigned out = 1;
    for (a = 1; a < times.size(); a++)
    {
        double diff = times[a] - times[out - 1];
        if (diff >= 0.001)
        {
            times[out] = times[a];
            ++out;
        }
    }

    times.resize(out);
    printf("There are %ld unique timestamps from %d\n", times.size(), rdl.TotalTimes());

    // Cache the item indices for faster lookups
    unsigned lapItem            = rdl.FindItemByName("lapStartUTC");
    unsigned lapTimeItem        = rdl.FindItemByName("lapDuration");
    unsigned latitudeItem       = rdl.FindItemByName("GPS Latitude");
    unsigned longitudeItem      = rdl.FindItemByName("GPS Longitude");
    unsigned elevationItem      = rdl.FindItemByName("GPS Altitude");
    unsigned speedItem          = rdl.FindItemByName("GPS Velocity"); // In m/s
    unsigned headingItem        = rdl.FindItemByName("GPS Course");
    unsigned accelXItem         = rdl.FindItemByName("IMU Accel X");
    unsigned accelYItem         = rdl.FindItemByName("IMU Accel Y");
    unsigned rpmItem            = rdl.FindItemByName("ECU RPM");
    unsigned throttleItem       = rdl.FindItemByName("ECU TPS");

    // Resize the TelemetrySamples array and start filling it
    mSamples.reserve(times.size());

    for (a = 0; a < times.size(); a++)
    {
        if ((a % 100000) == 0) printf("Processing sample %d\n", a);

        TelemetrySample s;
        s.mLap            = rdl.ItemValueAtTime(lapItem, times[a]);
        s.mSectorIndex    = 0;
        s.mLapTime        = rdl.ItemValueAtTime(lapTimeItem, times[a]);
        s.mCumulativeTime = times[a];
        s.mLatitude       = rdl.ItemValueAtTime(latitudeItem,times[a]);
        s.mLongitude      = rdl.ItemValueAtTime(longitudeItem, times[a]);
        s.mElevation      = rdl.ItemValueAtTime(elevationItem, times[a]);
        //s.mSpeed          = rdl.ItemValueAtTime(speedItem, times[a]) * 3.6; // Convert from m/s to Kmh
        s.mSpeed          = rdl.ItemValueAtTime(speedItem, times[a]) * 2.23694; // Convert from m/s to mph
        s.mHeading        = rdl.ItemValueAtTime(headingItem, times[a]);
        s.mAccelX         = rdl.ItemValueAtTime(accelXItem, times[a]);
        s.mAccelY         = rdl.ItemValueAtTime(accelYItem, times[a]);
        s.mSectorTime     = 0;
        s.mRPM            = rdl.ItemValueAtTime(rpmItem, times[a]);
        s.mThrottle       = rdl.ItemValueAtTime(throttleItem, times[a]) * 0.5; // Throttle goes 0-200

        mSamples.push_back(s);
    }

    printf("Processing laps...\n");
    printf("There are %d samples in the laps\n", rdl.ItemSamples(lapItem));

    // Go through laps
    if (rdl.ItemSamples(lapItem) > 0)
    {
        unsigned lapStartIndex = 0;
        for (a = 0; a < rdl.ItemSamples(lapItem); a++)
        {
            TelemetryLap lap;
            lap.mLap = rdl.ItemSample(lapItem, a).value;
            printf("   Lap %d\n", lap.mLap);

            lap.mStartTime   = rdl.ItemSample(lapItem, a).time;
            lap.mDuration    = rdl.ItemSample(lapTimeItem, a).value;

            for (; (lapStartIndex < mSamples.size()) && (mSamples[lapStartIndex].mLap < lap.mLap); ++lapStartIndex) ;

            lap.mFirstSample = lapStartIndex;
            lap.mNumSamples  = -lapStartIndex;

            if (mLaps.size() > 0)
            {
                mLaps.back().mNumSamples += lapStartIndex;
            }

            mLaps.push_back(lap);
        }
        mLaps.back().mNumSamples += mSamples.size();
    }

    printf("Loaded %d laps, %d samples\n", (int)mLaps.size(), (int)mSamples.size());

#if 0
    FILE *outf = fopen("sampledump.txt", "wt");
    for (a = 0; a < mSamples.size(); a++)
    {
/**
    unsigned mLap;
    unsigned mSectorIndex;
    double   mLapTime;
    double   mCumulativeTime;
    double   mLatitude;
    double   mLongitude;
    double   mElevation;
    double   mSpeed;
    double   mHeading;
    double   mAccelX;
    double   mAccelY;
    double   mSectorTime;
    double   mRPM;
    double   mThrottle;
*/
        const TelemetrySample &ts = mSamples[a];

        fprintf(outf, "%7d CT:%f  RPM: %4d  TPS: %3d  VEL: %3d\n", a, ts.mCumulativeTime, (int)ts.mRPM, (int)ts.mThrottle, (int)ts.mSpeed);
    }
    fclose(outf);
#endif
    CleanupHeading(7.0);

    return true;
}

//---------------------------------------------------------
unsigned Telemetry::ExtractStrings(const char **strings, unsigned maxStrings, char *line)
{
    unsigned a;
    for (a = 0; a < maxStrings; a++) strings[a] = NULL;

    if (line == NULL) return 0;

    unsigned len = strlen(line);
    if (len == 0) return 0;

    // strip ending CR/LF from string
    while ((len > 0) && ((line[len - 1] == '\n') || (line[len - 1] == '\r')))
    {
        --len;
        line[len] = 0;
    }

    unsigned numStrings = 0;
    unsigned pos = 0;
    while (pos < len)
    {
        strings[numStrings] = &line[pos];
        ++numStrings;

        // Advance past the text of the entry, possibly blank!
        while ((pos < len) && (line[pos] != '\t')) ++pos;
        if (pos >= len) break;

        line[pos] = 0;
        ++pos; // move past the previous string
    }

    return numStrings;
}


//---------------------------------------------------------
double Telemetry::ShortestAngleDiff(double start, double end)
{
    double diff = end - start;
    if      (diff >  180.0) diff -= 360.0;
    else if (diff < -180.0) diff += 360.0;

    return diff;
}

//---------------------------------------------------------
// Run through and repair any broken headings
// The Solo seems to throw a few weird readings in a row every now and then
void Telemetry::CleanupHeading(double maxDiff)
{
    if (mSamples.size() < 3) return;

    double current = mSamples[0].mHeading;

    unsigned a;

    bool skipping = false;
    unsigned firstSkipped = 0;

    for (a = 1; a < mSamples.size(); a++)
    {
        double next = mSamples[a].mHeading;
        double diff = fabs(ShortestAngleDiff(current, next));
        if (skipping)
        {
            unsigned numSkipped = a - firstSkipped;
            diff /= (double) numSkipped;

        }
        if (diff > maxDiff)
        {
            //printf("Diff at sample %d is %7.3f  (C:%7.3f N:%7.3f)\n", a, diff, current, next);
            if (!skipping)
            {
                skipping = true;
                firstSkipped = a;
            }
        }
        else
        {
            if (skipping)
            {
                //printf("Skipped %d to %d\n", firstSkipped, a - 1);

                skipping = false;
                diff = ShortestAngleDiff(mSamples[firstSkipped - 1].mHeading, next);

                unsigned numSkipped = a - firstSkipped;
                double step = diff / (double) numSkipped;
                current = mSamples[firstSkipped - 1].mHeading;
                while (firstSkipped < a)
                {
                    current += step;
                    //printf("Updated sample %d from %7.3f to %7.3f\n", firstSkipped, mSamples[firstSkipped].mHeading, current);
                    mSamples[firstSkipped].mHeading = current;
                    ++firstSkipped;
                }
            }

            current = next;
        }
    }
}


//---------------------------------------------------------
unsigned Telemetry::FindStringIndex(const char **strings, unsigned numStrings, const char *match)
{
    unsigned a;
    for (a = 0; a < numStrings; a++)
    {
        if (0 == strcmp(match, strings[a])) return a;
    }

    printf("Cant match string '%s'\n", match);

    return 0xFFFF;
}


//---------------------------------------------------------
// extract seconds from a string formatted ' MM.SS.sss'
double Telemetry::GetSeconds(const char *string)
{
    char s[64];
    strncpy(s, string, 63);
    s[63] = 0;

    while ((strlen(s) > 0) && !isdigit(*s)) memmove(s, s + 1, strlen(s) + 1);
    s[2] = 0;
    double min = atof(s);
    double sec = atof(&s[3]);
    printf("min: %f ('%s') sec %f ('%s')\n", min, s, sec, &s[3]);
    return sec + (60.0 * min);
}


//---------------------------------------------------------
// Really dumb brute force
bool Telemetry::FindPair(TelemetrySample &pre, TelemetrySample &post, double time)
{
    unsigned a;
    for (a = 0; a < mSamples.size() - 1; a++)
    {
        if ((mSamples[a].mCumulativeTime <= time) && (mSamples[a+1].mCumulativeTime > time))
        {
            pre  = mSamples[a];
            post = mSamples[a+1];
            return true;
        }
    }
    return false;
}

//---------------------------------------------------------
unsigned Telemetry::FindClosestBefore(double time)
{
    if (mSamples.size() == 0) return 0;

    if (time >= mSamples.back().mCumulativeTime)  return mSamples.size() - 1;
    if (time <= mSamples.front().mCumulativeTime) return 0;

    unsigned low = 0;
    unsigned high = mSamples.size() - 1;
    unsigned mid;
    while (low < high)
    {
        mid = (low + high) / 2;
        double timeDiff = time - mSamples[mid].mCumulativeTime;
        if (fabs(timeDiff) < 0.001) return mid;
        else if (time < mSamples[mid].mCumulativeTime) high = mid - 1;
        else                                           low  = mid + 1;
    }

    while (mSamples[mid].mCumulativeTime > time) --mid;

    return mid;
}


//---------------------------------------------------------
double Telemetry::LapDuration(unsigned lap)
{
    unsigned a;
    for (a = 0; a < mLaps.size(); a++)
    {
        if (mLaps[a].mLap == lap) return mLaps[a].mDuration;
    }
    return 0.0;
}

//---------------------------------------------------------
#define DEG_TO_RAD (3.141592654 / 180.0)
double Telemetry::HaversineDistanceM(double lat1, double lon1, double lat2, double lon2)
{
    lat1 *= DEG_TO_RAD;
    lon1 *= DEG_TO_RAD;
    lat2 *= DEG_TO_RAD;
    lon2 *= DEG_TO_RAD;

    double dLat = lat2 - lat1;
    double dLon = lon2 - lon1;
    double a = (sin(dLat / 2.0) * sin(dLat / 2.0)) + (cos(lat1) * cos(lat2) * sin(dLon / 2.0) * sin(dLon / 2.0));

    return 6371000.0 * 2.0 * atan2(sqrt(a), sqrt(1.0-a));
}


//---------------------------------------------------------
double Telemetry::HaversineDistanceM2(double lat1, double lon1, double lat2, double lon2)
{
    double dx, dy, dz;
    lat1 -= lat2;
    lat1 *= DEG_TO_RAD;
    lon1 *= DEG_TO_RAD;
    lon2 *= DEG_TO_RAD;

    dz = sin(lon1) - sin(lon2);
    dx = cos(lat1) * cos(lon1) - cos(lon2);
    dy = sin(lat1) * cos(lon1);
    return asin(sqrt(dx * dx + dy * dy + dz * dz) / 2.0) * 2.0 * 6371000.0;
}

//---------------------------------------------------------
double Telemetry::ClosestApproachT(double latP, double lonP, double lat1, double lon1, double lat2, double lon2)
{
    double latD = lat2 - lat1;
    double lonD = lon2 - lon1;
    double lenD = sqrt((latD * latD) + (lonD * lonD));
    latD /= lenD;
    lonD /= lenD;

    double latPD = latP - lat1;
    double lonPD = lonP - lon1;

    double t = (latPD * latD) + (lonPD * lonD);
    return t / lenD;
}


//---------------------------------------------------------
bool Telemetry::UpdateSectors(const Track &track)
{
    if (mUpdatedSectors) return mSectorsGood;

    if (!track.HasSectors())
    {
        mSectorsGood = false;
        mUpdatedSectors = true;
        return mSectorsGood;
    }

    const double testRadius = 20.0; // in meters

    unsigned sectorIndex = 0;  // Invalid sector
    unsigned numSectors = track.Sectors().size();

    double nextDist = 9999.0;
    double sectorStartTime = 0.0;

    unsigned a;
    for (a = 0; a < mSamples.size() - 1; a++)
    {
        unsigned testIndex = sectorIndex + 1;
        if (testIndex > numSectors) testIndex = 1;
        const TrackSector &ts = track.FindSector(testIndex);

        double dist = nextDist;
        nextDist = HaversineDistanceM2(ts.latitude, ts.longitude, mSamples[a+1].mLatitude, mSamples[a+1].mLongitude);

        if ((nextDist < testRadius) && (dist < testRadius))
        {
            //printf("Sample at %f, %f within radius\n", mSamples[a].mCumulativeTime, mSamples[a+1].mCumulativeTime);
            double t = ClosestApproachT(ts.latitude, ts.longitude, mSamples[a].mLatitude, mSamples[a].mLongitude, mSamples[a+1].mLatitude, mSamples[a+1].mLongitude);

            // Does the line between two samples pass the closest approach?
            if ((t >= 0.0) && (t < 1.0))
            {
                //printf("Interp sample %f\n", t);
                mSectorsGood = true;

                TelemetrySample sample;

                sample.Interp(mSamples[a], mSamples[a+1], t);
                sample.mSectorTime = sample.mCumulativeTime - sectorStartTime;
                sample.mSectorIndex = sectorIndex;
                mSamples.insert(mSamples.begin() + a + 1, sample);

                sectorStartTime = sample.mCumulativeTime;
                sectorIndex = testIndex;
                ++a; // Skip the sample we just added
                continue;
            }
        }

        mSamples[a].mSectorIndex = sectorIndex;
        mSamples[a].mSectorTime  = mSamples[a].mCumulativeTime - sectorStartTime;
    }

    mUpdatedSectors = true;
    return mSectorsGood;
}





//---------------------------------------------------------
void Telemetry::Dump(void)
{
    printf("Summary for %s\n", mLoadedFile);
    unsigned a;
    for (a = 0; a < mLaps.size(); a++)
    {
        printf("  Lap %3d  Duration: %7.2fs  Start: %7.2fs\n", mLaps[a].mLap, mLaps[a].mDuration, mLaps[a].mStartTime);
    }
}
