#include "racedashlog.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "butterworth.h"

#define LOG_HEADER_MAGIC     0x52444C42 // 'RLDB'
#define LOG_TIME_INDEX_MAGIC 0x4C475449 // 'LGTI'

#define LOG_VERSION      0x000002

const char *RacedashLog::LogTypeNames[8] = { "Double", "Float", "U32", "S32", "U16", "S16", "U8", "S8" };

//-----------------------------------------------
RacedashLog::RacedashLog(const char *fname) :
    mTotalSamples(0),
    mTotalTimeIndices(0)
{
    FILE *inf = fopen(fname, "rt");
    if (inf == NULL) return;

    int ignore;

    // Check the file and make sure the magic is correct
    LogHeader header;
    ignore = fread(&header, sizeof(header), 1, inf);
    if (header.magic != LOG_HEADER_MAGIC)
    {
        printf("Log magic mismatch, read 0x%08X shd have been 0x%08X\n", header.magic, LOG_HEADER_MAGIC);
        return;
    }

    if (header.version != LOG_VERSION)
    {
        printf("Log version does not match should be %d, was %d\n", LOG_VERSION, header.version);
        return;
    }
    // Load the variable info
    unsigned a;
    for (a = 0; a < header.numVariables; a++)
    {
        LogHeaderVariable var;
        ignore = fread(&var, sizeof(var), 1, inf);
        LogItem item;
        item.name    = strdup(var.name);
        item.units   = strdup(var.units);
        item.scale   = 1.0 / var.scale;
        item.type    = var.type;
        item.id      = var.id;
        item.display = true;
        item.lastFoundIndex = 0;
        mLogItems.push_back(item);
    }

    // load the samples
    bool done = false;
    while (!feof(inf) && !done)
    {
        uint32_t magic;
        ignore = fread(&magic, sizeof(magic), 1, inf);
        fseek(inf, -sizeof(magic), SEEK_CUR);

        switch(magic)
        {
            case LOG_TIME_INDEX_MAGIC:
                done = !LoadLogTimeIndex(inf);
                break;

            default:
                printf("Got a bad Magic: 0x%08X\n", magic);
                done = true;
                break;
        }
    }

    fclose(inf);

    PostProcess();

}

//-----------------------------------------------
RacedashLog::~RacedashLog()
{
    mTotalSamples = 0;
    while (!mLogItems.empty())
    {
        LogItem &i = mLogItems.back();
        free(i.name);
        free(i.units);
        mLogItems.pop_back();
    }
}

//-----------------------------------------------
bool RacedashLog::LoadLogTimeIndex(FILE *inf)
{
    ++mTotalTimeIndices;
    struct LogTimeIndex lti;
    int ignore = fread(&lti, sizeof(lti), 1, inf);

    unsigned a;
    for (a = 0; a < lti.numVariables; a++)
    {
        DataLoggerID id;
        ignore = fread(&id, sizeof(id), 1, inf);

        int index = FindLogItemByID(id);
        if (index < 0) return false;

        LogItem &li = mLogItems[index];
        double out = 0;
        switch (li.type)
        {
            case Double:
            {
                double v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case Float:
            {
                float v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U32:
            {
                uint32_t v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S32:
            {
                int32_t v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U16:
            {
                uint16_t v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S16:
            {
                int16_t v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U8:
            {
                uint8_t v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S8:
            {
                int8_t v;
                ignore = fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
        }

        Sample s;
        s.time = lti.time * 0.0001;
        s.value = out * li.scale;
        if (s.time >1000000.0)
        {
            li.samples.push_back(s);
            ++mTotalSamples;
        }
        else
        {
            printf("Sample dropped on %s, invalid time %f\n", li.name, s.time);
        }
    }
    return true;
}

//----------------------------------------------------
int RacedashLog::FindLogItemByID(DataLoggerID id)
{
    unsigned a;
    for (a = 0; a < mLogItems.size(); a++)
    {
        if (mLogItems[a].id == id) return (int) a;
    }
    return -1;
}

//----------------------------------------------------
int RacedashLog::ItemIndexAtTime(unsigned itemIndex, double time) const
{
    if (itemIndex < 0) return 0.0;
    if (itemIndex >= mLogItems.size()) return 0.0;

    if (mLogItems[itemIndex].samples.empty()) return -1;

    const std::vector<Sample> &samples = mLogItems[itemIndex].samples;
    double first = samples[0].time;
    double last  = samples[samples.size() - 1].time;

    if (first >= time) return 0;
    if (last  <= time) return samples.size() - 1;

    unsigned low = 0;
    unsigned high = samples.size() - 1;
    unsigned mid;

    while (low < high)
    {
        mid = (low + high) / 2;
        double timeDiff = time - samples[mid].time;

        if (fabs(timeDiff) < 0.001) return mid;
        else if (time < samples[mid].time) high = mid - 1;
        else                               low  = mid + 1;
    }

    while (samples[mid].time > time) --mid;

    return mid;
}

//----------------------------------------------------
double RacedashLog::ItemValueAtTime(unsigned itemIndex, double time) const
{
    if (itemIndex < 0) return 0.0;
    if (itemIndex >= mLogItems.size()) return 0.0;

    const std::vector<Sample> &samples = mLogItems[itemIndex].samples;
    if (samples.empty()) return 0.0;

    if (time <  samples.front().time) return 0.0;
    if (time >= samples.back().time)  return samples.back().value;

    unsigned index = ItemIndexAtTime(itemIndex, time);
    double val = samples[index].value;

    if (samples[index].time < time)
    {
        double t = (time - samples[index].time) / (samples[index + 1].time - samples[index].time);
        val += (t * (samples[index + 1].value - val));
    }
    return val;
}

//----------------------------------------------------
int RacedashLog::FindItemByName(const char *name)
{
    if (name == NULL) return -1;
    unsigned a;
    for (a = 0; a < NumItems(); a++)
    {
        if (0 == strcmp(name, ItemName(a)))
        {
            return a;
        }
    }
    return -1;
}

//----------------------------------------------------
struct FilterItem
{
    const char *item;
    int order;
    double fc;
};

//----------------------------------------------------
static FilterItem sFilterItems[] =
{
    { "IMU Accel X", 12, 0.025 },
    { "IMU Accel X", 6, 0.015 },
    { "IMU Accel Y", 12, 0.025 },
    { "IMU Accel Y", 6, 0.015 },
    { "IMU Accel Z", 12, 0.025 },
    { "IMU Accel Z", 6, 0.015 },
    { NULL, 0, 0 },

};

//----------------------------------------------------
void RacedashLog::PostProcess(void)
{
    unsigned a;
    for (a = 0; sFilterItems[a].item != NULL; a++)
    {
        int index = FindItemByName(sFilterItems[a].item);
        if (index < 0) continue;

        LogItem &li = mLogItems[index];

        ButterworthLP filter(sFilterItems[a].order, sFilterItems[a].fc);

        printf("Postprocessing %s, Order: %d  Fc %.4f\n", sFilterItems[a].item, sFilterItems[a].order, sFilterItems[a].fc);

        unsigned b;
        for (b = 0; b < li.samples.size(); b++)
        {
            double out = filter.Process(li.samples[b].value);
            li.samples[b].value = out;
        }
    }
}
