#include <stdint.h>
#include <limits.h>
#include <vector>

#ifndef TELEMETRY_H
#define TELEMETRY_H

class Track;

//---------------------------------------------------------
class TelemetryLap
{
  public:
    unsigned mLap;
    unsigned mFirstSample;
    unsigned mNumSamples;

    double   mDuration;
    double   mStartTime;
};


//---------------------------------------------------------
class TelemetrySample
{
  public:
    unsigned mLap;
    unsigned mSectorIndex;
    double   mLapTime;
    double   mCumulativeTime;
    double   mLatitude;
    double   mLongitude;
    double   mElevation;
    double   mSpeed;
    double   mHeading;
    double   mAccelX;
    double   mAccelY;
    double   mSectorTime;
    double   mRPM;
    double   mThrottle;

    void Interp(const TelemetrySample &a, const TelemetrySample &b, double t)
    {
        mLap            = a.mLap;
        mSectorIndex    = a.mSectorIndex;
        mLapTime        = Interp(a.mLapTime, b.mLapTime, t);
        mCumulativeTime = Interp(a.mCumulativeTime, b.mCumulativeTime, t);
        mLatitude       = Interp(a.mLatitude, b.mLatitude, t);
        mLongitude      = Interp(a.mLongitude, b.mLongitude, t);
        mElevation      = Interp(a.mElevation, b.mElevation, t);
        mSpeed          = Interp(a.mSpeed, b.mSpeed, t);
        mHeading        = Interp(a.mHeading, b.mHeading, t);
        mAccelX         = Interp(a.mAccelX, b.mAccelX, t);
        mAccelY         = Interp(a.mAccelY, b.mAccelY, t);
        mSectorTime     = Interp(a.mSectorTime, b.mSectorTime, t);
        mRPM            = Interp(a.mRPM, b.mRPM, t);
        mThrottle       = Interp(a.mThrottle, b.mThrottle, t);
    }

  private:
    double Interp(double a, double b, double t)
    {
        return a + (t * (b - a));
    }

};


//---------------------------------------------------------
class Telemetry
{
  public:
    Telemetry(void);
    virtual ~Telemetry();

    bool Load(const char *fname);
    void Unload(void);

    void Dump(void);

    bool FindPair(TelemetrySample &pre, TelemetrySample &post, double time);
    double LapDuration(unsigned lap);

    unsigned FindClosestBefore(double time);
    const TelemetrySample &Sample(unsigned index) const { return (index < mSamples.size()) ? mSamples[index] : mSamples.back(); }
    unsigned SampleCount(void) const { return mSamples.size(); }

    bool UpdateSectors(const Track &track);
    bool SectorsGood(void) const { return mSectorsGood; }

    double StartTime(void) const { return mSamples.front().mCumulativeTime; }

  private:

    bool     LoadSoloExport(const char *fname);
    bool     LoadRacedash(const char *fname);

    void     CleanupHeading(double maxDiff);
    double   ShortestAngleDiff(double start, double end);

    unsigned ExtractStrings(const char **strings, unsigned maxStrings, char *line);
    unsigned FindStringIndex(const char **strings, unsigned numStrings, const char *match);
    double   GetSeconds(const char *string);
    double   HaversineDistanceM(double lat1, double lon1, double lat2, double lon2);
    double   HaversineDistanceM2(double lat1, double lon1, double lat2, double lon2);

    double   ClosestApproachT(double latP, double lonP, double lat1, double lon1, double lat2, double lon2);


    char mLoadedFile[PATH_MAX];

    std::vector<TelemetryLap>    mLaps;
    std::vector<TelemetrySample> mSamples;
    bool mUpdatedSectors;
    bool mSectorsGood;
};

#endif // TELEMETRY_H
